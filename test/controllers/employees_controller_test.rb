require 'test_helper'

class EmployeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @employee = employees(:one)
  end

  test "should get index" do
    get employees_url
    assert_response :success
  end

  test "should get new" do
    get new_employee_url
    assert_response :success
  end

  test "should create employee" do
    assert_difference('Employee.count') do
      post employees_url, params: { employee: { account: @employee.account, bank: @employee.bank, cellphone: @employee.cellphone, company_id: @employee.company_id, dni: @employee.dni, emergency_phone: @employee.emergency_phone, last_name: @employee.last_name, name: @employee.name, number_account: @employee.number_account, phone: @employee.phone } }
    end

    assert_redirected_to employee_url(Employee.last)
  end

  test "should show employee" do
    get employee_url(@employee)
    assert_response :success
  end

  test "should get edit" do
    get edit_employee_url(@employee)
    assert_response :success
  end

  test "should update employee" do
    patch employee_url(@employee), params: { employee: { account: @employee.account, bank: @employee.bank, cellphone: @employee.cellphone, company_id: @employee.company_id, dni: @employee.dni, emergency_phone: @employee.emergency_phone, last_name: @employee.last_name, name: @employee.name, number_account: @employee.number_account, phone: @employee.phone } }
    assert_redirected_to employee_url(@employee)
  end

  test "should destroy employee" do
    assert_difference('Employee.count', -1) do
      delete employee_url(@employee)
    end

    assert_redirected_to employees_url
  end
end
