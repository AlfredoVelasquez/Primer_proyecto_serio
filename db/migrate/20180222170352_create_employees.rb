class CreateEmployees < ActiveRecord::Migration[5.1]
  def change
    create_table :employees do |t|
      t.string :name
      t.string :last_name
      t.integer :dni
      t.integer :cellphone
      t.string :bank
      t.string :account
      t.string :number_account
      t.string :phone
      t.string :emergency_phone
      t.references :department, foreign_key: true

      t.timestamps
    end
  end
end
