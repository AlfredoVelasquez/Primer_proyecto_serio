class EmployeesController < ApplicationController
  before_action :set_employee, only: [:show, :edit, :update, :destroy]
  # before_action :assign_department

  # GET /employees
  # GET /employees.json
  def index
    @employees = Employee.where(department_id: params[:department_id])
    @employee = Employee.new
    @department = Department.find(params[:department_id])
  end

  # GET /employees/1
  # GET /employees/1.json
  def show
    @department = Department.find(params[:department_id])
  end

  # GET /employees/new
  def new
    @employee = Employee.new
    @department = Department.find(params[:department_id])
  end

  # GET /employees/1/edit
  def edit
    @employees = Employee.where(department_id: params[:department_id])
    @department = Department.find(params[:department_id])
  end

  # POST /employees
  # POST /employees.json
  def create
    @employee = Employee.new(employee_params)
    @department = Department.find(params[:department_id])
    # @employee = @department.build_employee(employee_params.merge!{:department_id})

    respond_to do |format|
      if @employee.save
        format.html { redirect_to department_employees_path, notice: 'El empleado fue creado exitosamente' }
        format.json { render :show, status: :created, location: @employee }
      else
        format.html { render :new }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /employees/1
  # PATCH/PUT /employees/1.json
  def update
    respond_to do |format|
      if @employee.update(employee_params)
        format.html { redirect_to department_employees_path, notice: 'El empleado fue editado exitosamente' }
        format.json { render :show, status: :ok, location: @employee }
      else
        format.html { render :edit }
        format.json { render json: @employee.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /employees/1
  # DELETE /employees/1.json
  def destroy
    @employee.destroy
    respond_to do |format|
      format.html { redirect_to department_employees_path, notice: 'El empleado fue borrado exitosamente' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_employee
      @employee = Employee.find(params[:id])
    end

    # def assign_department
    #   @department = Department.find(params[:department_id])
    # end

    # Never trust parameters from the scary internet, only allow the white list through.
    def employee_params
      params.require(:employee).permit(:name, :last_name, :dni, :cellphone, :bank, :account, :number_account, :phone, :emergency_phone, :department_id)
    end
end
