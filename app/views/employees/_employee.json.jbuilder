json.extract! employee, :id, :name, :last_name, :dni, :cellphone, :bank, :account, :number_account, :phone, :emergency_phone, :company_id, :created_at, :updated_at
json.url employee_url(employee, format: :json)
